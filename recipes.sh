#! /bin/bash

Settings="/etc/sddm.conf.d/kde_settings.conf"


post_install () {
	if [[ -f "${Settings}" ]]; then
		sed --in-place 's|Current=breeze|Current=dawn|' "${Settings}"
	fi
}


pre_remove () {
	if [[ -f "${Settings}" ]]; then
		sed --in-place 's|Current=dawn|Current=breeze|' "${Settings}"
	fi
}

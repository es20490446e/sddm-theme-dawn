
BACKGROUND:
(cc-by-sa-4.0) 2022 Solen Feyissa
Gave license permission by email
https://unsplash.com/photos/r0lYekmCf4E

LOCK ICON:
(CC0) 2021 Vijay Verma
https://commons.wikimedia.org/wiki/File:Lock-dynamic-gradient.png

KEYBOARD ICON:
(cc-by-3.0) 2024 Yode Tive
https://thenounproject.com/icon/keyboard-6684318/

SESSION ICON:
(cc-by-3.0) 2024 Culai Lai
https://thenounproject.com/icon/eye-6785645/

FACE:
(Latest cc-by-sa) 2022 Alberto Salvia Novella (es20490446e)
Generated using thispersondoesnotexist.com

CODE:
Latest GNU General Public Licence
- Breeze Theme: David Edmundson
- Chili Theme: Marian Alexander Arlt
- Dawn Theme: Alberto Salvia Novella
- Keyboard Layout: Daniel Vrátil
- User Delegate: Aleix Pol Gonzalez
- Virtual Keyboard: Martin Gräßlin


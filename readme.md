![1.jpg](https://gitlab.com/es20490446e/sddm-theme-dawn/-/raw/main/root/usr/share/sddm/themes/dawn/components/images/screenshots/1.jpg)

![2.jpg](https://gitlab.com/es20490446e/sddm-theme-dawn/-/raw/main/root/usr/share/sddm/themes/dawn/components/images/screenshots/2.jpg)

![3.jpg](https://gitlab.com/es20490446e/sddm-theme-dawn/-/raw/main/root/usr/share/sddm/themes/dawn/components/images/screenshots/3.jpg)

Compatible with any desktop environment.

Also available on:
- KDE System Settings -> Startup and Shutdown -> Login Screen (SDDM)
- [Pling](https://www.pling.com/p/1939296)
- [The Express Repository](https://gitlab.com/es20490446e/express/-/wikis/home)


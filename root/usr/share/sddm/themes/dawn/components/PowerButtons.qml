import QtQuick

Row {
	PowerButton {
		iconSource: "images/icons/reboot.svgz"
		text: "<font color='" + fontColor + "'>" + textConstants.reboot + "</font>"
		enabled: sddm.canReboot
		onClicked: sddm.reboot()
	}

	PowerButton {
		iconSource: "images/icons/shutdown.svgz"
		text: "<font color='" + fontColor + "'>" + textConstants.shutdown + "</font>"
		enabled: sddm.canPowerOff
		onClicked: sddm.powerOff()
	}
}

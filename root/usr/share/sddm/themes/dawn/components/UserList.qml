import QtQuick

ListView {
    id: view

    readonly property string selectedUser: currentItem ? currentItem.userName : ""
    readonly property int userItemWidth: gridUnit * 25
    readonly property int userItemHeight: userItemWidth
    readonly property bool constrainText: count > 1

    signal userSelected

    implicitHeight: userItemHeight
    orientation: ListView.Horizontal
    highlightRangeMode: ListView.StrictlyEnforceRange

    preferredHighlightBegin: width/2 - userItemWidth/2
    preferredHighlightEnd: preferredHighlightBegin
    interactive: count > 1

    delegate: UserDelegate {
        avatarPath: model.icon || ""
        iconSource: model.iconName || "user-identity"

        name: model.realName || model.name
        userName: model.name

        width: userItemWidth
        height: userItemHeight

        constrainText: ListView.view.count > 1
        isCurrent: ListView.isCurrentItem

        onClicked: {
            ListView.view.currentIndex = index
            ListView.view.userSelected()
        }
    }

    Keys.onEscapePressed: view.userSelected()
    Keys.onEnterPressed: view.userSelected()
    Keys.onReturnPressed: view.userSelected()
}

import QtQuick
import QtQuick.Controls

Item {
    id: button

    property alias containsMouse: mouseArea.containsMouse
    property alias font: label.font
    property alias text: label.text
    property string iconSource

    signal clicked

    width: gridUnit * 12
    height: gridUnit * 9
    opacity: containsMouse ? 0.9 : 0.5

    Keys.onEnterPressed: clicked()
    Keys.onReturnPressed: clicked()
    Keys.onSpacePressed: clicked()

    Accessible.onPressAction: clicked()
    Accessible.role: Accessible.Button
    Accessible.name: label.text

    MouseArea {
        id: mouseArea
        hoverEnabled: true
        onClicked: button.clicked()
        anchors.fill: button
    }

    Column {
        id: column
        anchors.horizontalCenter: parent.horizontalCenter

        Image {
            source: iconSource
            width: gridUnit * 6
            height: gridUnit * 6
            sourceSize.width: width * 2
            sourceSize.height: height * 2
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            id: label

            font {
                family: config.font || "sans-serif"
                underline: mouseArea.activeFocus
                pointSize: fontSize
            }
        }
    }
}

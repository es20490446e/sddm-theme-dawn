import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window
import SddmComponents

Item {
    id: root

    property string notificationMessage
    property string fontColor: config.fontColor || "#ffffff"
    property int gridUnit: Screen.desktopAvailableHeight/100
    property int fontSize: Screen.desktopAvailableHeight/65

    TextConstants {
        id: textConstants
    }

    Loader {
        source: "Background.qml"
    }

    StackView {
        id: mainStack
        height: root.height
        focus: true

        anchors {
            left: parent.left
            right: parent.right
        }

        initialItem: Login {
            id: userListComponent
            userListModel: userModel
            userListCurrentIndex: userModel.lastIndex >= 0 ? userModel.lastIndex : 0

            showUserList: {
                if (!userListModel.hasOwnProperty("count") ||
                userListModel.hasOwnProperty("disableAvatarsThreshold")) {
                    return (userList.y + mainStack.y) > 0
                } else if ( userListModel.count == 0 ) {
                    return false
                } else {
                    return userListModel.count <= userListModel.disableAvatarsThreshold &&
                    (userList.y + mainStack.y) > 0
                }
            }

            notificationMessage: {
                var text = ""

                if (keyboard.capsLock) {
                    text += textConstants.capslockWarning

                    if (root.notificationMessage) {
                        text += " • "
                    }
                }

                text += root.notificationMessage
                return text
            }

            onLoginRequest: function (username, password) {
                root.notificationMessage = ""
                sddm.login(username, password, sessionButton.currentIndex)
            }
        }
    }

    PowerButtons {
        anchors.horizontalCenter: root.horizontalCenter
        y: parent.height - height - gridUnit*2
    }

    VirtualKeyboard {
        anchors.fill: parent
    }

    ColumnLayout {
        anchors {
            bottom: parent.bottom
            bottomMargin: gridUnit
            left: parent.left
            leftMargin: gridUnit
        }

        KeyboardButton {
            id: keyboardButton
        }

        SessionButton {
            id: sessionButton
        }
    }

    Connections {
        target: sddm

        function onLoginFailed() {
            notificationMessage = textConstants.loginFailed
        }

        function onLoginSucceeded() {
            // here SDDM will kill the greeter at some random point
            // hence there is no certainty that any transition will finish
            mainStack.opacity = 0
            footer.opacity = 0
        }
    }

    onNotificationMessageChanged: {
        if (notificationMessage) {
            notificationResetTimer.start();
        }
    }

    Timer {
        id: notificationResetTimer
        interval: 3000
        onTriggered: notificationMessage = ""
    }
}

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

ToolButton {
    id: toolButton

    property int currentIndex: -1
    property string option: instantiator.objectAt(currentIndex).text
    property string stability: option.indexOf("Wayland") != -1 ? "(EXPERIMENTAL)" : ""

    signal sessionChanged()

    visible: menu.count > 1
    opacity: 0.8
    implicitWidth: icon.width + label.width + fontSize * 3.5
    implicitHeight: fontSize * 3

    onClicked: menu.open()
    Component.onCompleted: currentIndex = sessionModel.lastIndex

    Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: parent.hovered || parent.activeFocus ? 0.5 : 0.2
        radius: 6
    }

    RowLayout {
        id: layout
        spacing: fontSize * 0.5

        anchors {
            fill: parent
            leftMargin: fontSize * 1.2
        }

        Image {
            id: icon
            source: "images/icons/session.svgz"
            width: fontSize * 2
            sourceSize.width: width
        }

        Text {
            id: label
            text: "<font color='" + "black" + "'>" + option + " " + stability + "</font>"
            Layout.leftMargin: layout.spacing

            font {
                family: config.font || "sans-serif"
                pointSize: fontSize
            }
        }
    }

    Menu {
        id: menu

        Instantiator {
            id: instantiator
            model: sessionModel

            onObjectAdded: function (index, object) {
                menu.insertItem(index, object)
            }

            onObjectRemoved: function (object) {
                menu.removeItem(object)
            }

            delegate: MenuItem {
                text: model.name

                onTriggered: {
                    toolButton.currentIndex = model.index
                    sessionChanged()
                }

                font {
                    family: config.font || "sans-serif"
                    pointSize: fontSize
                }
            }
        }
    }
}

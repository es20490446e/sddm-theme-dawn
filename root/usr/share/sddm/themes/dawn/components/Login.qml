import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

SessionManagementScreen {
    property Item mainPasswordBox: passwordBox
    property bool showUserBox: !showUserList
    property bool passwordFieldOutlined: config.passwordOutlined == "true"

    signal loginRequest(string username, string password)

    function focusLogin() {
        if (showUserBox) {
            userBox.forceActiveFocus()
        } else {
            passwordBox.forceActiveFocus()
        }
    }

    function selectLogin() {
        if (showUserBox) {
            userBox.selectAll()
            userBox.forceActiveFocus()
        } else {
            passwordBox.selectAll()
            passwordBox.forceActiveFocus()
        }
    }

    function startLogin() {
        var username = showUserBox ? userBox.text : userList.selectedUser
        var password = passwordBox.text

        loginRequest(username, password)
    }

    TextField {
        id: userBox

        font {
            family: config.font || "sans-serif"
            pointSize: fontSize * 1.4
        }

        implicitHeight: showUserBox ? fontSize * 4 : fontSize * 3
        leftPadding: fontSize * 1.5
        rightPadding: leftPadding

        Layout.fillWidth: true
        Layout.minimumHeight: implicitHeight

        opacity: 0.75
        focus: showUserBox
        visible: showUserBox

        //fieldStyle
        color: passwordFieldOutlined ? "white" : "black"
        placeholderText: textConstants.userName
        background: Rectangle {
            radius: 10
            border.color: "white"
            border.width: 1
            color: passwordFieldOutlined ? "transparent" : "white"
        }
    }

    TextField {
        id: passwordBox

        font {
            family: config.font || "sans-serif"
            pointSize: showUserBox ? fontSize * 1.2 : fontSize * 0.9
        }

        implicitHeight: showUserBox ? fontSize * 4 : fontSize * 3
        leftPadding: fontSize * 1.5
        rightPadding: leftPadding

        Layout.fillWidth: true
        Layout.minimumHeight: implicitHeight

        opacity: 0.75
        focus: !showUserBox
        echoMode: TextInput.Password
        onAccepted: startLogin()

        //fieldStyle
        color: passwordFieldOutlined ? "white" : "black"
        placeholderText: showUserBox ? textConstants.password : ""
        passwordCharacter: "●"
        background: Rectangle {
            radius: 10
            border.color: "white"
            border.width: 1
            color: passwordFieldOutlined ? "transparent" : "white"
        }

        Keys.onEscapePressed: {
            mainStack.currentItem.forceActiveFocus()
        }

        Keys.onPressed: function (event) {
            if (event.key === Qt.Key_Left && !text) {
                userList.decrementCurrentIndex()
                event.accepted = true
            }

            if (event.key === Qt.Key_Right && !text) {
                userList.incrementCurrentIndex()
                event.accepted = true
            }
        }

        Connections {
            target: keyboardButton

            function onKeyboardChanged() {
                focusLogin()
            }
        }

        Connections {
            target: sessionButton

            function onSessionChanged() {
                focusLogin()
            }
        }

        Connections {
            target: sddm

            function onLoginFailed() {
                selectLogin()
            }
        }
    }
}

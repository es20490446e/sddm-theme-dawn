import QtQuick
import QtQuick.VirtualKeyboard

Item {
	InputPanel {
		id: virtualKeyboard

		y: parent.height
		anchors.left: parent.left
		anchors.right: parent.right

		states: State {
				name: "visible"
				when: virtualKeyboard.active

				PropertyChanges {
					target: virtualKeyboard
					y: parent.height - virtualKeyboard.height
				}
		}

		transitions: Transition {
			from: ""
			to: "visible"
			reversible: true

			NumberAnimation {
				properties: "y"
				duration: 250
				easing.type: Easing.InOutQuad
			}
		}
	}
}

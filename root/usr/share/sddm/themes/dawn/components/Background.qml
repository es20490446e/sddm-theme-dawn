import QtQuick

Item {
	width: root.width
	height: root.height

	Rectangle {
		anchors.fill: parent
		color: "grey"
	}

	Image {
		anchors.fill: parent
		source: config.background || "images/background/blurred.jpg"
		fillMode: Image.PreserveAspectCrop
	}
}

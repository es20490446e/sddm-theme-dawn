import QtQuick
import QtQuick.Controls
import QtQuick.Effects

Item {
    id: wrapper

    property bool isCurrent: true
    readonly property var m: model
    property string name
    property string userName
    property string avatarPath
    property string iconSource
    property bool constrainText: true

    signal clicked()
    property real faceSize: gridUnit*20
    opacity: isCurrent ? 1.0 : 0.25

    Behavior on opacity {
        OpacityAnimator {
            duration: 400
        }
    }

    Item {
        id: imageSource
        width: faceSize
        height: faceSize

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: gridUnit *3
        }

        Rectangle {
            id: mask
            anchors.centerIn: parent
            color: "#b3b3b3"
            width: faceSize
            height: faceSize
            radius: 180
            visible: false
        }


        Rectangle {
            id: background
            anchors.centerIn: parent
            color: "#afdde9"
            width: faceSize
            height: faceSize
            radius: 180
            visible: false
        }

        MultiEffect {
            opacity: 0.3
            source: background
            anchors.fill: background
            maskEnabled: true
            maskSource: ShaderEffectSource {
                sourceItem: mask
            }
        }

        Image {
            id: face
            source: wrapper.avatarPath
            sourceSize: Qt.size(faceSize, faceSize)
            visible: false
        }

        MultiEffect {
            source: face
            anchors.fill: face
            maskEnabled: true
            maskSource: ShaderEffectSource {
                sourceItem: mask
            }
        }

        Rectangle {
            id: border
            anchors.centerIn: parent
            color: "transparent"
            border.color: "#ececec"
            border.width: 3
            width: faceSize + 4
            height: faceSize + 4
            radius: 180
        }
    }

    Text {
        id: usernameDelegate

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: imageSource.bottom
        }

        font {
            family: config.font || "sans-serif"
            capitalization: Font.Capitalize
            underline: wrapper.activeFocus
            pointSize: fontSize*1.1
        }

        color: config.fontColor || "#ffffff"
        text: wrapper.name
        wrapMode: Text.WordWrap
        renderType: Text.QtRendering
        horizontalAlignment: Text.AlignHCenter
        width: constrainText ? parent.width : implicitWidth
        maximumLineCount: wrapper.constrainText ? 2 : 1
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: wrapper.clicked();
    }

    Accessible.name: name
    Accessible.role: Accessible.Button

    function accessiblePressAction() {
        wrapper.clicked()
    }
}

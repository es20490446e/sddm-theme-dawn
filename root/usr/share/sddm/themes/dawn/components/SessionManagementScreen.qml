import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {
    id: root

    default property alias _children: innerLayout.children
    property alias notificationMessage: notificationsLabel.text
    property bool showUserList: true
    property alias userList: userListView
    property alias userListCurrentIndex: userListView.currentIndex
    property var userListCurrentModelData: userListView.currentItem === null ? [] : userListView.currentItem.m
    property alias userListModel: userListView.model

    UserList {
        id: userListView
        visible: showUserList && y > 0

        anchors {
            bottom: parent.verticalCenter
            left: parent.left
            right: parent.right
        }
    }

    Image {
        visible: !showUserList
        source: config.userIcon ? config.userIcon : "images/icons/lock.png"
        fillMode: Image.PreserveAspectFit
        height: gridUnit * 20
        sourceSize.height: height * 2

        anchors {
            bottom: parent.verticalCenter
            left: parent.left
            right: parent.right
        }
    }

    ColumnLayout {
        anchors {
            top: parent.top
            topMargin: gridUnit * 53
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        ColumnLayout {
            Layout.minimumHeight: implicitHeight
            Layout.maximumHeight: showUserList ? gridUnit * 12 : gridUnit * 15
            Layout.maximumWidth: gridUnit * 50
            Layout.alignment: Qt.AlignHCenter

            ColumnLayout {
                id: innerLayout
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
            }
        }

        Label {
            id: notificationsLabel
            color: "#ffeeaa"
            Layout.maximumWidth: gridUnit * 50
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.fillHeight: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignTop
            wrapMode: Text.WordWrap

            font {
                bold: true
                pointSize: fontSize * 1.2
            }
        }
    }
}
